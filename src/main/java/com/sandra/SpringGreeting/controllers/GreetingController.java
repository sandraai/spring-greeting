package com.sandra.SpringGreeting.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    private String generalGreeting1 = "Hello ";
    private String generalGreeting2 = ", finally spring is here. Best season of the year.";

    @GetMapping("/greeting")
    public String greeting () {
        return generalGreeting1 + "Sandra" + generalGreeting2;
    }

    @GetMapping("/greeting/{name}")
    public String greetingPathVariable (@PathVariable String name) {
        return generalGreeting1 + name + generalGreeting2;
    }
    
    @RequestMapping(value = "/palindrome", method = RequestMethod.GET)
    public String palindrome (@RequestParam("query") String inputString) {
        StringBuilder stringBuilder = new StringBuilder(inputString);
        if (inputString.equals(stringBuilder.reverse().toString()))  {
            return "That word is a palindrome.";
        }
        return "That is not a palindrome.";
    }

}
